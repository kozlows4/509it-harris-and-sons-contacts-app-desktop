﻿
namespace _509it_harris_and_sons_contacts_app_desktop.Forms
{
    partial class BaseForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.mainMenuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.businessContactsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.personalContactsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addBusinessContactToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addPersonalContactToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mainMenuToolStripMenuItem,
            this.businessContactsToolStripMenuItem,
            this.personalContactsToolStripMenuItem,
            this.addBusinessContactToolStripMenuItem,
            this.addPersonalContactToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1498, 28);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // mainMenuToolStripMenuItem
            // 
            this.mainMenuToolStripMenuItem.BackColor = System.Drawing.Color.White;
            this.mainMenuToolStripMenuItem.Name = "mainMenuToolStripMenuItem";
            this.mainMenuToolStripMenuItem.Size = new System.Drawing.Size(97, 24);
            this.mainMenuToolStripMenuItem.Text = "Main Menu";
            this.mainMenuToolStripMenuItem.Click += new System.EventHandler(this.mainMenuToolStripMenuItem_Click);
            // 
            // businessContactsToolStripMenuItem
            // 
            this.businessContactsToolStripMenuItem.BackColor = System.Drawing.Color.White;
            this.businessContactsToolStripMenuItem.Name = "businessContactsToolStripMenuItem";
            this.businessContactsToolStripMenuItem.Size = new System.Drawing.Size(139, 24);
            this.businessContactsToolStripMenuItem.Text = "Business Contacts";
            this.businessContactsToolStripMenuItem.Click += new System.EventHandler(this.businessContactsToolStripMenuItem_Click);
            // 
            // personalContactsToolStripMenuItem
            // 
            this.personalContactsToolStripMenuItem.BackColor = System.Drawing.Color.White;
            this.personalContactsToolStripMenuItem.Name = "personalContactsToolStripMenuItem";
            this.personalContactsToolStripMenuItem.Size = new System.Drawing.Size(139, 24);
            this.personalContactsToolStripMenuItem.Text = "Personal Contacts";
            this.personalContactsToolStripMenuItem.Click += new System.EventHandler(this.personalContactsToolStripMenuItem_Click);
            // 
            // addBusinessContactToolStripMenuItem
            // 
            this.addBusinessContactToolStripMenuItem.BackColor = System.Drawing.Color.White;
            this.addBusinessContactToolStripMenuItem.Name = "addBusinessContactToolStripMenuItem";
            this.addBusinessContactToolStripMenuItem.Size = new System.Drawing.Size(165, 24);
            this.addBusinessContactToolStripMenuItem.Text = "Add Business Contact";
            this.addBusinessContactToolStripMenuItem.Click += new System.EventHandler(this.addBusinessContactToolStripMenuItem_Click);
            // 
            // addPersonalContactToolStripMenuItem
            // 
            this.addPersonalContactToolStripMenuItem.BackColor = System.Drawing.Color.White;
            this.addPersonalContactToolStripMenuItem.Name = "addPersonalContactToolStripMenuItem";
            this.addPersonalContactToolStripMenuItem.Size = new System.Drawing.Size(165, 24);
            this.addPersonalContactToolStripMenuItem.Text = "Add Personal Contact";
            this.addPersonalContactToolStripMenuItem.Click += new System.EventHandler(this.addPersonalContactToolStripMenuItem_Click);
            // 
            // BaseForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1498, 516);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "BaseForm";
            this.Text = "BaseForm";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem mainMenuToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem businessContactsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem personalContactsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addBusinessContactToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addPersonalContactToolStripMenuItem;
    }
}