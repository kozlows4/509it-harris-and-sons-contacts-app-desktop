﻿using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using _509it_harris_and_sons_contacts_app_desktop.Lib.Models;
using _509it_harris_and_sons_contacts_app_desktop.UserControls;

namespace _509it_harris_and_sons_contacts_app_desktop.Forms
{
    public partial class BaseForm : Form
    {
        /// <summary>
        /// Menu constants
        /// </summary>
        public const string MainMenu = "main_menu"; 
        public const string BusinessContactsList = "business_contacts_list"; 
        public const string PersonalContactsList = "personal_contacts_list"; 
        public const string AddEditBusinessContact = "add_edit_business_contact"; 
        public const string AddEditPersonalContact = "add_edit_personal_contact";

        /// <summary>
        /// UserControl objects
        /// </summary>
        private readonly MainMenuControl _mainMenuControl;
        private readonly ContactsList _businessContactsListControl;
        private readonly ContactsList _personalContactsListControl;
        private readonly AddEditContact _addEditBusinessContactControl;
        private readonly AddEditContact _addEditPersonalContactControl;

        /// <summary>
        /// Lists of togglable panels and menu buttons 
        /// </summary>
        private readonly List<KeyValuePair<string, UserControl>> _panels;
        private readonly List<KeyValuePair<string, ToolStripMenuItem>> _menuItems;

        /// <summary>
        /// Contact models
        /// </summary>
        private readonly Model _businessContact = new BusinessContact();
        private readonly Model _personalContact = new PersonalContact();

        public BaseForm()
        {
            InitializeComponent();
            
            // Create instances of UserControls
            _mainMenuControl = new MainMenuControl(this)
            {
                Dock = DockStyle.Fill,
                Visible = false
            };

            _businessContactsListControl = new BusinessContactsListControl(this, _businessContact)
            {
                Dock = DockStyle.Fill,
                Visible = false
            };

            _personalContactsListControl = new PersonalContactsListControl(this, _personalContact)
            {
                Dock = DockStyle.Fill,
                Visible = false
            };

            _addEditBusinessContactControl = new AddEditBusinessContactControl(this, _businessContact)
            {
                Dock = DockStyle.Fill,
                Visible = false
            };

            _addEditPersonalContactControl = new AddEditPersonalContactControl(this, _personalContact)
            {
                Dock = DockStyle.Fill,
                Visible = false
            };

            // Set references to forms and contacts lists
            _businessContactsListControl.SetAddEditFormReference(ref _addEditBusinessContactControl);
            _personalContactsListControl.SetAddEditFormReference(ref _addEditPersonalContactControl);
            _addEditBusinessContactControl.SetContactsListReference(ref _businessContactsListControl);
            _addEditPersonalContactControl.SetContactsListReference(ref _personalContactsListControl);

            // Collect all togglable panels
            _panels = new List<KeyValuePair<string, UserControl>>
            {
                new KeyValuePair<string, UserControl>(MainMenu, _mainMenuControl),
                new KeyValuePair<string, UserControl>(BusinessContactsList, _businessContactsListControl),
                new KeyValuePair<string, UserControl>(PersonalContactsList, _personalContactsListControl),
                new KeyValuePair<string, UserControl>(AddEditBusinessContact, _addEditBusinessContactControl),
                new KeyValuePair<string, UserControl>(AddEditPersonalContact, _addEditPersonalContactControl)
            };

            // Collect all menu buttons
            _menuItems = new List<KeyValuePair<string, ToolStripMenuItem>>
            {
                new KeyValuePair<string, ToolStripMenuItem>(MainMenu, mainMenuToolStripMenuItem),
                new KeyValuePair<string, ToolStripMenuItem>(BusinessContactsList, businessContactsToolStripMenuItem),
                new KeyValuePair<string, ToolStripMenuItem>(PersonalContactsList, personalContactsToolStripMenuItem),
                new KeyValuePair<string, ToolStripMenuItem>(AddEditBusinessContact, addBusinessContactToolStripMenuItem),
                new KeyValuePair<string, ToolStripMenuItem>(AddEditPersonalContact, addPersonalContactToolStripMenuItem)
            };

            // Add UserControls to the form
            for (int i = 0, j = _panels.Count; i < j; i++)
            {
                this.Controls.Add(_panels[i].Value);
            }

            // Show main menu panel
            _mainMenuControl.Open();
        }

        /// <summary>
        /// Switch displayed panel
        /// </summary>
        /// <param name="panel"></param>
        /// <param name="resetMainMenu"></param>
        public void TogglePanel(string panel, bool resetMainMenu = false)
        {
            for (int i = 0, j = _panels.Count; i < j; i++)
            {
                _panels[i].Value.Visible = _panels[i].Key == panel;
            }

            if (resetMainMenu)
            {
                for (int i = 0, j = _menuItems.Count; i < j; i++)
                {
                    _menuItems[i].Value.BackColor = Color.White;
                }
            }
            else
            {
                for (int i = 0, j = _menuItems.Count; i < j; i++)
                {
                    _menuItems[i].Value.BackColor = (_menuItems[i].Key == panel) ? Color.CornflowerBlue : Color.White;
                }
            }
        }

        /// <summary>
        /// Change form's title
        /// </summary>
        /// <param name="title"></param>
        public void SetFormTitle(string title)
        {
            Text = title;
        }

        private void mainMenuToolStripMenuItem_Click(object sender, System.EventArgs e)
        {
            _mainMenuControl.Open();
        }

        private void businessContactsToolStripMenuItem_Click(object sender, System.EventArgs e)
        {
            _businessContactsListControl.Open();
        }

        private void personalContactsToolStripMenuItem_Click(object sender, System.EventArgs e)
        {
            _personalContactsListControl.Open();
        }

        private void addBusinessContactToolStripMenuItem_Click(object sender, System.EventArgs e)
        {
            _addEditBusinessContactControl.Open();
        }

        private void addPersonalContactToolStripMenuItem_Click(object sender, System.EventArgs e)
        {
            _addEditPersonalContactControl.Open();
        }
    }
}
