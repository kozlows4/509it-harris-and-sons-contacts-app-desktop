﻿using MySqlConnector;
using System;
using System.Configuration;
using System.Data;
using _509it_harris_and_sons_contacts_app_desktop.Lib.Utils;
using _509it_harris_and_sons_contacts_app_desktop.Lib.Exceptions.Handlers;

namespace _509it_harris_and_sons_contacts_app_desktop.Lib.Database
{
    public class Db
    {
        private static readonly string ConnectionString;

        /// <summary>
        /// Get database configuration
        /// </summary>
        static Db()
        {
            try
            {
                ConnectionString = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
            }
            catch (NullReferenceException)
            {
                DbExceptionHandler.ShowException("Database configuration couldn't be found!");
                throw;
            }
        }

        /// <summary>
        /// Read data from the database, handle exceptions
        /// </summary>
        /// <param name="queryData"></param>
        /// <returns></returns>
        public static DataTable Query(QueryData queryData)
        {
            try
            {
                return RunQuery(queryData);
            }
            catch (MySqlException) 
            {
                DbExceptionHandler.ShowException("Database configuration is invalid!");
                throw;
            }
        }

        /// <summary>
        /// Execute query inserting, updating or deleting data from the database, handle exceptions
        /// </summary>
        /// <param name="queryData"></param>
        /// <returns></returns>
        public static bool NonQuery(QueryData queryData)
        {

            try
            {
                return RunNonQuery(queryData);
            }
            catch (MySqlException)
            {
                DbExceptionHandler.ShowException("Database configuration is invalid!");
                throw;
            }
        }

        /// <summary>
        /// Read data from the database, may throw exceptions
        /// </summary>
        /// <param name="queryData"></param>
        /// <returns></returns>
        private static DataTable RunQuery(QueryData queryData)
        {
            using (MySqlConnection connection = new MySqlConnection(ConnectionString))
            {
                connection.Open();

                using (MySqlCommand command = new MySqlCommand(queryData.Query, connection))
                {
                    for (int i = 0, j = queryData.Parameters.Count; i < j; i++)
                    {
                        command.Parameters.AddWithValue(queryData.Parameters[i].Key, queryData.Parameters[i].Value);
                    }

                    using (MySqlDataReader reader = command.ExecuteReader())
                    {
                        DataTable results = new DataTable();
                        results.Load(reader);

                        return results;
                    }
                }
            }
        }

        /// <summary>
        /// Execute query inserting, updating or deleting data from the database, may throw exceptions
        /// </summary>
        /// <param name="queryData"></param>
        /// <returns></returns>
        private static bool RunNonQuery(QueryData queryData)
        {
            using (MySqlConnection connection = new MySqlConnection(ConnectionString))
            {
                connection.Open();

                using (MySqlCommand command = new MySqlCommand(queryData.Query, connection))
                {
                    for (int i = 0, j = queryData.Parameters.Count; i < j; i++)
                    {
                        command.Parameters.AddWithValue(queryData.Parameters[i].Key, queryData.Parameters[i].Value);
                    }

                    Console.WriteLine(queryData.Query);

                    return command.ExecuteNonQuery() > 0;
                }
            }
        }
    }
}
