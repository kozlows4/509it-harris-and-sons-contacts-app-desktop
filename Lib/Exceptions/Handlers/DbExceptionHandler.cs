﻿using System;
using System.Windows.Forms;

namespace _509it_harris_and_sons_contacts_app_desktop.Lib.Exceptions.Handlers
{
    public class DbExceptionHandler
    {
        /// <summary>
        /// Show exception message and exit the application
        /// </summary>
        /// <param name="message"></param>
        public static void ShowException(string message)
        {
            if (MessageBox.Show(message, "Warning", MessageBoxButtons.OK, MessageBoxIcon.Stop) == DialogResult.OK)
            {
                Environment.Exit(-1);
            }
        }
    }
}
