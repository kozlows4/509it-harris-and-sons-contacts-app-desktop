﻿using System.Linq;

namespace _509it_harris_and_sons_contacts_app_desktop.Lib.Models
{
    public class BusinessContact : Contact
    {
        /// <summary>
        /// Search fields belonging solely to the business contacts
        /// </summary>
        private static readonly string[] OwnSearchableColumns =
        {
            "business_phone_area_code",
            "business_phone_number",
            "company",
            "position"
        };

        public BusinessContact()
        {
            Table = "business_contacts";
            IdColumn = "id";
            SearchableColumns = SharedSearchableColumns.Concat(OwnSearchableColumns).ToArray();
        }
    }
}
