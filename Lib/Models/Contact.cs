﻿namespace _509it_harris_and_sons_contacts_app_desktop.Lib.Models
{
    public abstract class Contact : Model
    {
        /// <summary>
        /// Shared search columns
        /// </summary>
        protected static string[] SharedSearchableColumns =
        {
            "first_name",
            "last_name",
            "email",
            "phone_area_code",
            "phone_number",
            "address_line_1",
            "address_line_2",
            "address_line_3",
            "postcode",
            "city",
            "country",
        };
    }
}
