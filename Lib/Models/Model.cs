﻿using System.Collections.Generic;
using System.Data;
using _509it_harris_and_sons_contacts_app_desktop.Lib.Database;
using _509it_harris_and_sons_contacts_app_desktop.Lib.Utils;
using KeyValuePair = System.Collections.Generic.KeyValuePair<string, object?>;

namespace _509it_harris_and_sons_contacts_app_desktop.Lib.Models
{
    public abstract class Model
    {
        /// <summary>
        /// ORDER BY constants
        /// </summary>
        private const string ASCENDING = "ASC";
        private const string DESCENDING = "ASC";

        /// <summary>
        /// Name of the table used in queries
        /// </summary>
        protected string Table;
        
        /// <summary>
        /// Name of the primary key column, usually "id"
        /// </summary>
        protected string IdColumn;
        
        /// <summary>
        /// Array containing names of all columns used to search for specified values
        /// </summary>
        protected string[] SearchableColumns;

        /// <summary>
        /// Get multiple rows from the database
        /// </summary>
        /// <param name="search"></param>
        /// <param name="limit"></param>
        /// <param name="offset"></param>
        /// <param name="orderColumn"></param>
        /// <param name="sortAscending"></param>
        /// <returns></returns>
        public DataTable Get(string search, int limit, int offset, string orderColumn, bool sortAscending)
        {
            return Db.Query(
                GetSelectQueryData(search, limit, offset, orderColumn, sortAscending)
            );
        }

        /// <summary>
        /// Get single matching row from the database
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public DataTable Find(int id)
        {
            return Db.Query(
                GetSelectQueryData(id)    
            );
        }

        /// <summary>
        /// Delete single row from the database
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool Delete(int id)
        {
            return Db.NonQuery(
                GetDeleteQueryData(id)    
            );
        }

        /// <summary>
        /// Insert new record into the database
        /// </summary>
        /// <param name="values"></param>
        /// <returns></returns>
        public bool Insert(List<KeyValuePair> values)
        {
            return Db.NonQuery(
                GetInsertQueryData(values)    
            );
        }

        /// <summary>
        /// Update existing record
        /// </summary>
        /// <param name="id"></param>
        /// <param name="values"></param>
        /// <returns></returns>
        public bool Update(int id, List<KeyValuePair> values)
        {
            return Db.NonQuery(
                GetUpdateQueryData(id, values)    
            );
        }

        /// <summary>
        /// Format parameterized search string
        /// </summary>
        /// <param name="search"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        private string GetSearchString(string search, ref List<KeyValuePair> parameters)
        {
            if (search.Length < 1)
            {
                return "";
            }
            else
            {
                string searchString = " WHERE ";

                for (int i = 0, j = SearchableColumns.Length; i < j; i++)
                {
                    if (i == 0)
                    {
                        searchString += (SearchableColumns[i] + " LIKE CONCAT(\"%\", @search" + i + ", \"%\")");
                    }
                    else
                    {
                        searchString += (" OR " + SearchableColumns[i] + " LIKE CONCAT(\"%\", @search" + i + ", \"%\")");
                    }

                    parameters.Add(
                        new KeyValuePair("@search" + i, search)
                    );
                }

                return searchString;
            }
        }

        /// <summary>
        /// Prepare SELECT query to get multiple records from the database
        /// </summary>
        /// <param name="search"></param>
        /// <param name="limit"></param>
        /// <param name="offset"></param>
        /// <param name="orderColumn"></param>
        /// <param name="sortAscending"></param>
        /// <returns></returns>
        private QueryData GetSelectQueryData(string search, int limit, int offset, string orderColumn, bool sortAscending)
        {
            List<KeyValuePair> parameters = new List<KeyValuePair>
            {
                new KeyValuePair("@limit", limit),
                new KeyValuePair("@offset", offset)
            };
            string query = "SELECT * FROM " +
               Table +
               GetSearchString(search, ref parameters) +
               " ORDER BY " +
               orderColumn +
               " " + 
               (sortAscending ? ASCENDING : DESCENDING) +
               " LIMIT @limit OFFSET @offset";


            return new QueryData()
            {
                Query = query,
                Parameters = parameters
            };
        }

        /// <summary>
        /// Prepare SELECT query to retrieve 1 specific record from the database
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        private QueryData GetSelectQueryData(int id)
        {
            return new QueryData()
            {
                Query = "SELECT * FROM " + Table + " WHERE " + IdColumn + " = @id LIMIT 1",
                Parameters = new List<KeyValuePair>
                {
                    new KeyValuePair("@id", id),
                }
            };
        }

        /// <summary>
        /// Prepare DELETE query to delete 1 specific record from teh database
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        private QueryData GetDeleteQueryData(int id)
        {
            return new QueryData()
            {
                Query = "DELETE FROM " + Table + " WHERE " + IdColumn + " = @id",
                Parameters = new List<KeyValuePair>
                {
                    new KeyValuePair("@id", id)
                }
            };
        }

        /// <summary>
        /// Prepare INSERT query to add 1 record to the database
        /// </summary>
        /// <param name="values"></param>
        /// <returns></returns>
        private QueryData GetInsertQueryData(List<KeyValuePair> values)
        {
            List<KeyValuePair> parameters = new List<KeyValuePair>();
            string queryColumns = " (" + IdColumn;
            string queryValues = " (NULL";

            for (int i = 0, j = values.Count; i < j; i++)
            {
                queryColumns += (", " + values[i].Key);
                queryValues += (", @" + values[i].Key);

                parameters.Add(new KeyValuePair("@" + values[i].Key, values[i].Value));
            }

            return new QueryData()
            {
                Query = "INSERT INTO " + Table + queryColumns + ") VALUES " + queryValues + ")",
                Parameters = parameters
            };
        }

        /// <summary>
        /// Prepare UPDATE query to modify 1 specific record in the database
        /// </summary>
        /// <param name="id"></param>
        /// <param name="values"></param>
        /// <returns></returns>
        private QueryData GetUpdateQueryData(int id, List<KeyValuePair> values)
        {
            List<KeyValuePair> parameters = new List<KeyValuePair>()
            {
                new KeyValuePair("@id", id)
            };
            string query = "UPDATE " + Table + " SET ";

            for (int i = 0, j = values.Count; i < j; i++)
            {
                if (i == 0)
                {
                    query += (values[i].Key + " = @" + values[i].Key);
                }
                else
                {
                    query += (", " + values[i].Key + " = @" + values[i].Key);
                }

                parameters.Add(new KeyValuePair("@" + values[i].Key, values[i].Value));
            }

            return new QueryData()
            {
                Query = query + " WHERE " + IdColumn + " = @id",
                Parameters = parameters
            };
        }
    }
}
