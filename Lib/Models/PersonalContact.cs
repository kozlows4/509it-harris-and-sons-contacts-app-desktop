﻿using System.Linq;

namespace _509it_harris_and_sons_contacts_app_desktop.Lib.Models
{
    public class PersonalContact : Contact
    {
        /// <summary>
        /// Search fields belonging solely to the personal contacts
        /// </summary>
        private static readonly string[] OwnSearchableColumns =
        {
            "personal_phone_area_code",
            "personal_phone_number"
        };

        public PersonalContact()
        {
            Table = "personal_contacts";
            IdColumn = "id";
            SearchableColumns = SharedSearchableColumns.Concat(OwnSearchableColumns).ToArray();
        }
    }
}
