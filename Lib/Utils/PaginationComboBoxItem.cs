﻿namespace _509it_harris_and_sons_contacts_app_desktop.Lib.Utils
{
    /// <summary>
    /// Store pagination dropdown item
    /// </summary>
    class PaginationComboBoxItem
    {
        public int Value { get; set; }
    }
}
