﻿using System.Collections.Generic;
using KeyValuePair = System.Collections.Generic.KeyValuePair<string, object?>;

namespace _509it_harris_and_sons_contacts_app_desktop.Lib.Utils
{
    /// <summary>
    /// Store query string and list of parameters
    /// </summary>
    public class QueryData
    {
        public string Query = "";
        public List<KeyValuePair> Parameters = new List<KeyValuePair>();
    }
}
