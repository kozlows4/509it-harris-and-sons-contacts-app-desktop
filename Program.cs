﻿using System;
using System.Windows.Forms;
using _509it_harris_and_sons_contacts_app_desktop.Forms;

namespace _509it_harris_and_sons_contacts_app_desktop
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new BaseForm());
        }
    }
}
