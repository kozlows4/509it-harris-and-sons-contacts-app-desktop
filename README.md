Harris & Sons Contact Management App - Windows Forms
---
Last updated: 13 December 2020

Author: [Jakub Kozlowski](kozlows4@cucollege.coventry.ac.uk)

Module: 509IT - Advanced Software Development


## Description

Windows Forms-based implementation of the contact management app. 

### Development Environment
#### Required software 
1. XAMPP (or equivalent software including MySQL database server)
2. IDE of choice
#### Setup
1. Import or create a database
2. Copy "Database.config.example" file to "Database.config" and enter local configuration for database access