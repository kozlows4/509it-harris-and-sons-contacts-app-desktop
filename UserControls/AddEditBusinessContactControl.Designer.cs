﻿
namespace _509it_harris_and_sons_contacts_app_desktop.UserControls
{
    partial class AddEditBusinessContactControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label1 = new System.Windows.Forms.Label();
            this.firstName = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.lastName = new System.Windows.Forms.TextBox();
            this.addressLine3 = new System.Windows.Forms.TextBox();
            this.postcode = new System.Windows.Forms.TextBox();
            this.city = new System.Windows.Forms.TextBox();
            this.phoneAreaCode = new System.Windows.Forms.TextBox();
            this.email = new System.Windows.Forms.TextBox();
            this.businessPhoneNumber = new System.Windows.Forms.TextBox();
            this.businessPhoneAreaCode = new System.Windows.Forms.TextBox();
            this.addressLine2 = new System.Windows.Forms.TextBox();
            this.position = new System.Windows.Forms.TextBox();
            this.addressLine1 = new System.Windows.Forms.TextBox();
            this.phoneNumber = new System.Windows.Forms.TextBox();
            this.company = new System.Windows.Forms.TextBox();
            this.country = new System.Windows.Forms.TextBox();
            this.errorProvider = new System.Windows.Forms.ErrorProvider(this.components);
            this.submitButton = new System.Windows.Forms.Button();
            this.cancelButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(8, 62);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(76, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "First Name";
            // 
            // firstName
            // 
            this.firstName.Location = new System.Drawing.Point(11, 82);
            this.firstName.MaxLength = 50;
            this.firstName.Name = "firstName";
            this.firstName.Size = new System.Drawing.Size(171, 22);
            this.firstName.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(8, 107);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(76, 17);
            this.label2.TabIndex = 2;
            this.label2.Text = "Last Name";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(557, 197);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(58, 17);
            this.label3.TabIndex = 3;
            this.label3.Text = "Position";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(557, 107);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(164, 17);
            this.label4.TabIndex = 4;
            this.label4.Text = "Business Phone Number";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(557, 152);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(67, 17);
            this.label5.TabIndex = 5;
            this.label5.Text = "Company";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(280, 287);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(57, 17);
            this.label6.TabIndex = 6;
            this.label6.Text = "Country";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(280, 107);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(103, 17);
            this.label7.TabIndex = 7;
            this.label7.Text = "Address Line 2";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(280, 62);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(103, 17);
            this.label8.TabIndex = 8;
            this.label8.Text = "Address Line 1";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(8, 197);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(174, 17);
            this.label9.TabIndex = 9;
            this.label9.Text = "Phone Number Area Code";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(280, 197);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(67, 17);
            this.label10.TabIndex = 10;
            this.label10.Text = "Postcode";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(557, 62);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(235, 17);
            this.label11.TabIndex = 11;
            this.label11.Text = "Business Phone Number Area Code";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(280, 242);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(31, 17);
            this.label12.TabIndex = 12;
            this.label12.Text = "City";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(280, 152);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(103, 17);
            this.label13.TabIndex = 13;
            this.label13.Text = "Address Line 3";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(8, 242);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(103, 17);
            this.label14.TabIndex = 14;
            this.label14.Text = "Phone Number";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(8, 152);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(42, 17);
            this.label15.TabIndex = 15;
            this.label15.Text = "Email";
            // 
            // lastName
            // 
            this.lastName.Location = new System.Drawing.Point(11, 127);
            this.lastName.MaxLength = 50;
            this.lastName.Name = "lastName";
            this.lastName.Size = new System.Drawing.Size(171, 22);
            this.lastName.TabIndex = 2;
            // 
            // addressLine3
            // 
            this.addressLine3.Location = new System.Drawing.Point(283, 172);
            this.addressLine3.MaxLength = 255;
            this.addressLine3.Name = "addressLine3";
            this.addressLine3.Size = new System.Drawing.Size(171, 22);
            this.addressLine3.TabIndex = 8;
            // 
            // postcode
            // 
            this.postcode.Location = new System.Drawing.Point(283, 217);
            this.postcode.MaxLength = 10;
            this.postcode.Name = "postcode";
            this.postcode.Size = new System.Drawing.Size(171, 22);
            this.postcode.TabIndex = 9;
            // 
            // city
            // 
            this.city.Location = new System.Drawing.Point(283, 262);
            this.city.MaxLength = 255;
            this.city.Name = "city";
            this.city.Size = new System.Drawing.Size(171, 22);
            this.city.TabIndex = 10;
            // 
            // phoneAreaCode
            // 
            this.phoneAreaCode.Location = new System.Drawing.Point(11, 217);
            this.phoneAreaCode.MaxLength = 5;
            this.phoneAreaCode.Name = "phoneAreaCode";
            this.phoneAreaCode.Size = new System.Drawing.Size(171, 22);
            this.phoneAreaCode.TabIndex = 4;
            // 
            // email
            // 
            this.email.Location = new System.Drawing.Point(11, 172);
            this.email.MaxLength = 255;
            this.email.Name = "email";
            this.email.Size = new System.Drawing.Size(171, 22);
            this.email.TabIndex = 3;
            // 
            // businessPhoneNumber
            // 
            this.businessPhoneNumber.Location = new System.Drawing.Point(560, 127);
            this.businessPhoneNumber.MaxLength = 20;
            this.businessPhoneNumber.Name = "businessPhoneNumber";
            this.businessPhoneNumber.Size = new System.Drawing.Size(171, 22);
            this.businessPhoneNumber.TabIndex = 13;
            // 
            // businessPhoneAreaCode
            // 
            this.businessPhoneAreaCode.Location = new System.Drawing.Point(560, 82);
            this.businessPhoneAreaCode.MaxLength = 5;
            this.businessPhoneAreaCode.Name = "businessPhoneAreaCode";
            this.businessPhoneAreaCode.Size = new System.Drawing.Size(171, 22);
            this.businessPhoneAreaCode.TabIndex = 12;
            // 
            // addressLine2
            // 
            this.addressLine2.Location = new System.Drawing.Point(283, 127);
            this.addressLine2.MaxLength = 255;
            this.addressLine2.Name = "addressLine2";
            this.addressLine2.Size = new System.Drawing.Size(171, 22);
            this.addressLine2.TabIndex = 7;
            // 
            // position
            // 
            this.position.Location = new System.Drawing.Point(560, 217);
            this.position.MaxLength = 255;
            this.position.Name = "position";
            this.position.Size = new System.Drawing.Size(171, 22);
            this.position.TabIndex = 15;
            // 
            // addressLine1
            // 
            this.addressLine1.Location = new System.Drawing.Point(283, 82);
            this.addressLine1.MaxLength = 255;
            this.addressLine1.Name = "addressLine1";
            this.addressLine1.Size = new System.Drawing.Size(171, 22);
            this.addressLine1.TabIndex = 6;
            // 
            // phoneNumber
            // 
            this.phoneNumber.Location = new System.Drawing.Point(11, 262);
            this.phoneNumber.MaxLength = 20;
            this.phoneNumber.Name = "phoneNumber";
            this.phoneNumber.Size = new System.Drawing.Size(171, 22);
            this.phoneNumber.TabIndex = 5;
            // 
            // company
            // 
            this.company.Location = new System.Drawing.Point(560, 172);
            this.company.MaxLength = 255;
            this.company.Name = "company";
            this.company.Size = new System.Drawing.Size(171, 22);
            this.company.TabIndex = 14;
            // 
            // country
            // 
            this.country.Location = new System.Drawing.Point(283, 307);
            this.country.MaxLength = 255;
            this.country.Name = "country";
            this.country.Size = new System.Drawing.Size(171, 22);
            this.country.TabIndex = 11;
            // 
            // errorProvider
            // 
            this.errorProvider.BlinkRate = 500;
            this.errorProvider.BlinkStyle = System.Windows.Forms.ErrorBlinkStyle.NeverBlink;
            this.errorProvider.ContainerControl = this;
            // 
            // submitButton
            // 
            this.submitButton.Location = new System.Drawing.Point(283, 346);
            this.submitButton.Name = "submitButton";
            this.submitButton.Size = new System.Drawing.Size(75, 23);
            this.submitButton.TabIndex = 31;
            this.submitButton.Text = "Submit";
            this.submitButton.UseVisualStyleBackColor = true;
            this.submitButton.Click += new System.EventHandler(this.submitButton_Click);
            // 
            // cancelButton
            // 
            this.cancelButton.Location = new System.Drawing.Point(379, 346);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(75, 23);
            this.cancelButton.TabIndex = 32;
            this.cancelButton.Text = "Cancel";
            this.cancelButton.UseVisualStyleBackColor = true;
            this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
            // 
            // AddEditBusinessContactControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.submitButton);
            this.Controls.Add(this.country);
            this.Controls.Add(this.company);
            this.Controls.Add(this.phoneNumber);
            this.Controls.Add(this.addressLine1);
            this.Controls.Add(this.position);
            this.Controls.Add(this.addressLine2);
            this.Controls.Add(this.businessPhoneAreaCode);
            this.Controls.Add(this.businessPhoneNumber);
            this.Controls.Add(this.email);
            this.Controls.Add(this.phoneAreaCode);
            this.Controls.Add(this.city);
            this.Controls.Add(this.postcode);
            this.Controls.Add(this.addressLine3);
            this.Controls.Add(this.lastName);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.firstName);
            this.Controls.Add(this.label1);
            this.Name = "AddEditBusinessContactControl";
            this.Size = new System.Drawing.Size(1205, 530);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox firstName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox lastName;
        private System.Windows.Forms.TextBox addressLine3;
        private System.Windows.Forms.TextBox postcode;
        private System.Windows.Forms.TextBox city;
        private System.Windows.Forms.TextBox phoneAreaCode;
        private System.Windows.Forms.TextBox email;
        private System.Windows.Forms.TextBox businessPhoneNumber;
        private System.Windows.Forms.TextBox businessPhoneAreaCode;
        private System.Windows.Forms.TextBox addressLine2;
        private System.Windows.Forms.TextBox position;
        private System.Windows.Forms.TextBox addressLine1;
        private System.Windows.Forms.TextBox phoneNumber;
        private System.Windows.Forms.TextBox company;
        private System.Windows.Forms.TextBox country;
        private System.Windows.Forms.ErrorProvider errorProvider;
        private System.Windows.Forms.Button submitButton;
        private System.Windows.Forms.Button cancelButton;
    }
}
