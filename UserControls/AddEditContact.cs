﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net.Mail;
using System.Windows.Forms;
using _509it_harris_and_sons_contacts_app_desktop.Forms;
using _509it_harris_and_sons_contacts_app_desktop.Lib.Models;
using KeyValuePair = System.Collections.Generic.KeyValuePair<string, object?>;

namespace _509it_harris_and_sons_contacts_app_desktop.UserControls
{
    public class AddEditContact : UserControl
    {
        /// <summary>
        /// Base form and Contact model
        /// </summary>
        protected BaseForm BaseForm;
        protected Model Contact;
        protected ContactsList ContactsList;

        /// <summary>
        /// Inputs and error provider
        /// </summary>
        protected List<KeyValuePair<string, TextBox>> Inputs;
        protected ErrorProvider ErrorProvider;
        protected Button SubmitButton;

        /// <summary>
        /// ID of edited contact
        /// </summary>
        protected int SelectedContact = 0;

        /// <summary>
        /// List of nullable fields - if empty will be set to NULL
        /// </summary>
        private static readonly string[] NullableFields =
        {
            "address_line_2",
            "address_line_3",
        };

        /// <summary>
        /// Set reference to the ContactList UserControl instance
        /// </summary>
        /// <param name="contactsList"></param>
        public void SetContactsListReference(ref ContactsList contactsList)
        {
            ContactsList = contactsList;
        }

        /// <summary>
        /// Display the UserControl
        /// </summary>
        /// <param name="contactId"></param>
        public void Open(int contactId = 0)
        {
            SelectedContact = contactId;

            ToggleBaseFormPanel(
                IsEdit()
            );

            PopulateForm();
        }

        /// <summary>
        /// Switch displayed UserControl
        /// </summary>
        /// <param name="isEdit"></param>
        protected virtual void ToggleBaseFormPanel(bool isEdit)
        {

        }

        /// <summary>
        /// Handle form submission
        /// </summary>
        protected void SubmitButtonClicked()
        {
            if (!ValidateForm()) return;

            if (IsEdit())
            {
                UpdateContact();
            }
            else
            {
                InsertContact();
            }
        }

        /// <summary>
        /// Handle cancel button click
        /// </summary>
        protected void CancelButtonClicked()
        {
            ContactsList.Open(
                !IsEdit()
            );
        }

        /// <summary>
        /// Get data and populate form inputs
        /// </summary>
        private void PopulateForm()
        {
            ResetForm();

            if (SelectedContact == 0) return;

            DataTable contactDetails = Contact.Find(SelectedContact);

            if (contactDetails.Rows.Count < 1)
            {
                SelectedContact = 0;

                MessageBox.Show("Selected contact doesn't exist!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Stop);

                SubmitButton.Enabled = false;
            }
            else
            {
                SetInputsValues(contactDetails.Rows[0]);
            }
        }

        /// <summary>
        /// Check if add or edit form is opened
        /// </summary>
        /// <returns></returns>
        private bool IsEdit()
        {
            return SelectedContact > 0;
        }

        /// <summary>
        /// Reset form inputs' values
        /// </summary>
        /// <param name="resetSelectedContact"></param>
        private void ResetForm(bool resetSelectedContact = false)
        {
            SubmitButton.Enabled = true;

            if (resetSelectedContact)
            {
                SelectedContact = 0;
            }

            for (int i = 0, j = Inputs.Count; i < j; i++)
            {
                Inputs[i].Value.Text = "";
                ErrorProvider.SetError(Inputs[i].Value, "");
            }
        }

        /// <summary>
        /// Assign values to form inputs
        /// </summary>
        /// <param name="row"></param>
        private void SetInputsValues(DataRow row)
        {
            for (int i = 0, j = Inputs.Count; i < j; i++)
            {
                string dbValue = row[Inputs[i].Key].ToString();

                if (dbValue != "")
                {
                    Inputs[i].Value.Text = dbValue;
                }
            }
        }

        /// <summary>
        /// Validate form
        /// </summary>
        /// <returns></returns>
        private bool ValidateForm()
        {
            bool isValid = true;

            for (int i = 0, j = Inputs.Count; i < j; i++)
            {
                if (Inputs[i].Value.Text == "" && !NullableFields.Contains(Inputs[i].Key))
                {
                    isValid = false;
                    ErrorProvider.SetError(Inputs[i].Value, "Input mustn't be empty!");
                }
                else
                {
                    if (Inputs[i].Key == "email")
                    {
                        try
                        {
                            if (Inputs[i].Value.Text == new MailAddress(Inputs[i].Value.Text).Address)
                            {
                                ErrorProvider.SetError(Inputs[i].Value, "");
                            }
                        }
                        catch (Exception e)
                        {
                            isValid = false;
                            ErrorProvider.SetError(Inputs[i].Value, "Please enter a valid email address!");
                        }
                    }
                    else
                    {
                        ErrorProvider.SetError(Inputs[i].Value, "");
                    }
                }
            }

            return isValid;
        }

        /// <summary>
        /// Add new contact
        /// </summary>
        private void InsertContact()
        {
            if (Contact.Insert(GetFormData()))
            {
                DialogResult action = ShowInsertUpdateSuccessMessage(
                    "Contact has been added. Do you want to add another contact?"
                );

                if (action == DialogResult.Yes)
                {
                    ResetForm(true);
                }
                else
                {
                    ContactsList.Open();
                }
            }
            else
            {
                ShowInsertUpdateErrorMessage("Contact couldn't be added!");
            }
        }

        /// <summary>
        /// Update existing contact
        /// </summary>
        private void UpdateContact()
        {
            if (Contact.Update(SelectedContact, GetFormData()))
            {
                DialogResult action = ShowInsertUpdateSuccessMessage(
                    "Contact has been Updated. Do you want to make any other changes?"
                );

                if (action == DialogResult.No)
                {
                    ContactsList.Open(false);
                }
            }
            else
            {
                ShowInsertUpdateErrorMessage("Contact update failed!");
            }
        }

        /// <summary>
        /// Show success message
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        private DialogResult ShowInsertUpdateSuccessMessage(string message)
        {
            return MessageBox.Show(message, "Success", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
        }

        /// <summary>
        /// Show error message
        /// </summary>
        /// <param name="message"></param>
        private void ShowInsertUpdateErrorMessage(string message)
        {
            MessageBox.Show(message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        /// <summary>
        /// Get data from the form
        /// </summary>
        /// <returns></returns>
        private List<KeyValuePair> GetFormData()
        {
            List<KeyValuePair> formData = new List<KeyValuePair>();

            for (int i = 0, j = Inputs.Count; i < j; i++)
            {
                formData.Add(
                    new KeyValuePair(
                        Inputs[i].Key, 
                        (
                            Inputs[i].Value.Text == "" 
                            && NullableFields.Contains(Inputs[i].Key)
                        ) ? null : Inputs[i].Value.Text
                    )
                );
            }

            return formData;
        }
    }
}
