﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using _509it_harris_and_sons_contacts_app_desktop.Forms;
using _509it_harris_and_sons_contacts_app_desktop.Lib.Models;

namespace _509it_harris_and_sons_contacts_app_desktop.UserControls
{
    public partial class AddEditPersonalContactControl : AddEditContact
    {
        public AddEditPersonalContactControl(BaseForm form, Model contact)
        {
            BaseForm = form;
            Contact = contact;

            InitializeComponent();

            ErrorProvider = errorProvider;
            Inputs = new List<KeyValuePair<string, TextBox>>
            {
                new KeyValuePair<string, TextBox>("first_name", firstName),
                new KeyValuePair<string, TextBox>("last_name", lastName),
                new KeyValuePair<string, TextBox>("email", email),
                new KeyValuePair<string, TextBox>("phone_area_code", phoneAreaCode),
                new KeyValuePair<string, TextBox>("phone_number", phoneNumber),
                new KeyValuePair<string, TextBox>("address_line_1", addressLine1),
                new KeyValuePair<string, TextBox>("address_line_2", addressLine2),
                new KeyValuePair<string, TextBox>("address_line_3", addressLine3),
                new KeyValuePair<string, TextBox>("postcode", postcode),
                new KeyValuePair<string, TextBox>("city", city),
                new KeyValuePair<string, TextBox>("country", country),
                new KeyValuePair<string, TextBox>("home_phone_area_code", homePhoneAreaCode),
                new KeyValuePair<string, TextBox>("home_phone_number", homePhoneNumber),
            };

            SubmitButton = submitButton;
        }

        /// <summary>
        /// Switch displayed UserControl
        /// </summary>
        /// <param name="isEdit"></param>
        protected override void ToggleBaseFormPanel(bool isEdit)
        {
            BaseForm.TogglePanel(BaseForm.AddEditPersonalContact, isEdit);
            BaseForm.SetFormTitle(
                "Harris & Sons Consulting LTD - " +
                (isEdit ? "Edit" : "Add") +
                " Personal Contact"
            );
        }

        private void submitButton_Click(object sender, EventArgs e)
        {
            SubmitButtonClicked();
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            CancelButtonClicked();
        }
    }
}
