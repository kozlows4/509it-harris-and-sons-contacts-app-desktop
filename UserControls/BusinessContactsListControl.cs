﻿using System.Data;
using System.Linq;
using System.Windows.Forms;
using _509it_harris_and_sons_contacts_app_desktop.Forms;
using _509it_harris_and_sons_contacts_app_desktop.Lib.Models;

namespace _509it_harris_and_sons_contacts_app_desktop.UserControls
{
    public partial class BusinessContactsListControl : ContactsList
    {
        /// <summary>
        /// Columns that can be used to order the contacts
        /// </summary>
        private readonly int[] _sortableColumns = {0, 1, 2, 3, 10, 11};

        public BusinessContactsListControl(BaseForm form, Model contact)
        {
            BaseForm = form;
            Contact = contact;

            InitializeComponent();

            ContactsListTable = contactsList;
            ContactsListPaginateSelect = paginateSelect;
            ContactsListSearchBox = contactsSearch;

            PaginatePrevButton = paginatePrev;
            PaginateNextButton = paginateNext;
            PaginatePageLabel = paginatePageNumber;

            InitContactsListComponents();
        }

        /// <summary>
        /// Display UserControl
        /// </summary>
        protected override void ToggleBaseFormPanel()
        {
            BaseForm.TogglePanel(BaseForm.BusinessContactsList);
            BaseForm.SetFormTitle("Harris & Sons Consulting LTD - Business Contacts");
        }

        /// <summary>
        /// Format data to be displayed in the DataGridView
        /// </summary>
        /// <param name="retrievedContacts"></param>
        /// <returns></returns>
        protected override DataTable FormatQueryResult(DataTable retrievedContacts)
        {
            DataTable contacts = new DataTable();
            contacts.Columns.AddRange(new DataColumn[]
            {
                new DataColumn("id"),
                new DataColumn("first_name"),
                new DataColumn("last_name"),
                new DataColumn("email"),
                new DataColumn("phone"),
                new DataColumn("address"),
                new DataColumn("postcode"),
                new DataColumn("city"),
                new DataColumn("country"),
                new DataColumn("business_phone"),
                new DataColumn("company"),
                new DataColumn("position")
            });

            foreach (DataRow dbRow in retrievedContacts.Rows)
            {
                DataRow row = contacts.NewRow();

                row["id"] = (int)dbRow["id"];
                row["first_name"] = dbRow["first_name"];
                row["last_name"] = dbRow["last_name"];
                row["email"] = dbRow["email"];
                row["phone"] = FormatPhoneNumber(
                    dbRow["phone_area_code"].ToString(),
                    dbRow["phone_number"].ToString()
                );
                row["address"] = FormatAddress(
                    GetNullableString(dbRow, "address_line_1"),
                    GetNullableString(dbRow, "address_line_2"),
                    GetNullableString(dbRow, "address_line_3")
                );
                row["postcode"] = dbRow["postcode"].ToString();
                row["city"] = dbRow["city"].ToString();
                row["country"] = dbRow["country"].ToString();
                row["business_phone"] = FormatPhoneNumber(
                    dbRow["business_phone_area_code"].ToString(),
                    dbRow["business_phone_number"].ToString()
                );
                row["company"] = dbRow["company"].ToString();
                row["position"] = dbRow["position"].ToString();

                contacts.Rows.Add(row);
            }

            return contacts;
        }

        private void contactsList_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (!_sortableColumns.Contains(e.ColumnIndex)) return;

            SortableColumnHeaderClicked(e.ColumnIndex);
        }

        private void paginateSelect_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            PerPageNumberChanged();
        }

        private void contactsSearch_TextChanged(object sender, System.EventArgs e)
        {
            SearchChanged();
        }

        private void paginatePrev_Click(object sender, System.EventArgs e)
        {
            PrevButtonClicked();
        }

        private void paginateNext_Click(object sender, System.EventArgs e)
        {
            NextButtonClicked();
        }

        private void editButton_Click(object sender, System.EventArgs e)
        {
            EditButtonClicked();
        }

        private void deleteButton_Click(object sender, System.EventArgs e)
        {
            DeleteButtonClicked();
        }
    }
}
