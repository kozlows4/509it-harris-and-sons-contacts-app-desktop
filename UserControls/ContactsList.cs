﻿using System;
using System.Data;
using System.Windows.Forms;
using _509it_harris_and_sons_contacts_app_desktop.Forms;
using _509it_harris_and_sons_contacts_app_desktop.Lib.Models;
using _509it_harris_and_sons_contacts_app_desktop.Lib.Utils;

namespace _509it_harris_and_sons_contacts_app_desktop.UserControls
{
    public class ContactsList : UserControl
    {
        /// <summary>
        /// Base form and Contact model
        /// </summary>
        protected Model Contact;
        protected BaseForm BaseForm;
        protected AddEditContact EditContactControl;

        /// <summary>
        /// Reference to DataGridView
        /// </summary>
        protected DataGridView ContactsListTable;

        /// <summary>
        /// Reference to table's pagination dropdown
        /// </summary>
        protected ComboBox ContactsListPaginateSelect;

        /// <summary>
        /// Reference to table's search input
        /// </summary>
        protected TextBox ContactsListSearchBox;

        /// <summary>
        /// References to pagination controls
        /// </summary>
        protected Button PaginatePrevButton;
        protected Button PaginateNextButton;
        protected Label PaginatePageLabel;

        /// <summary>
        /// Sorting information
        /// </summary>
        private int _lastSortingColumnIndex = 0;
        private string _lastSortingColumnName = "id";
        private bool _sortAscending = true;

        /// <summary>
        /// Indicate if sorting has been changed
        /// </summary>
        private bool _updateSortingData = false;

        /// <summary>
        /// Set reference to the AddEditContact UserControl instance
        /// </summary>
        /// <param name="addEditForm"></param>
        public void SetAddEditFormReference(ref AddEditContact addEditForm)
        {
            EditContactControl = addEditForm;
        }

        /// <summary>
        /// Display the UserControl
        /// </summary>
        /// <param name="resetPagination"></param>
        public virtual void Open(bool resetPagination = true)
        {
            ToggleBaseFormPanel();

            UpdateContactsListData(resetPagination);
        }

        /// <summary>
        /// Switch displayed UserControl
        /// </summary>
        protected virtual void ToggleBaseFormPanel()
        {

        }

        /// <summary>
        /// Configure DataGridView and related controls
        /// </summary>
        protected void InitContactsListComponents()
        {
            ContactsListTable.AutoGenerateColumns = true;
            ContactsListTable.Enabled = true;
            ContactsListTable.Columns[5].DefaultCellStyle.WrapMode = DataGridViewTriState.True;
            ContactsListTable.ColumnHeadersDefaultCellStyle.WrapMode = DataGridViewTriState.False;
            ContactsListTable.Refresh();

            ContactsListPaginateSelect.DisplayMember= "Value";
            ContactsListPaginateSelect.ValueMember = "Value";
            ContactsListPaginateSelect.DataSource = new PaginationComboBoxItem[]
            {
                new PaginationComboBoxItem{Value = 10}, 
                new PaginationComboBoxItem{Value = 25}, 
                new PaginationComboBoxItem{Value = 50}, 
                new PaginationComboBoxItem{Value = 100} 
            };
        }

        /// <summary>
        /// Update data displayed in the DataGridView
        /// </summary>
        /// <param name="resetPagination"></param>
        private void UpdateContactsListData(bool resetPagination = false)
        {
            if (resetPagination)
            {
                ResetPagination();
            }
            
            DataTable retrievedContacts = GetContacts();
        
            UpdateNextPageStatus(retrievedContacts);

            ContactsListTable.DataSource = FormatQueryResult(retrievedContacts);
            ContactsListTable.Refresh();

            if (!_updateSortingData) return;

            ContactsListTable.Columns[_lastSortingColumnIndex].HeaderCell.SortGlyphDirection = _sortAscending
                ? SortOrder.Ascending
                : SortOrder.Descending;

            _updateSortingData = false;
        }

        /// <summary>
        /// Format phone number to be displayed in the DataGridView
        /// </summary>
        /// <param name="areaCode"></param>
        /// <param name="phoneNumber"></param>
        /// <returns></returns>
        protected static string FormatPhoneNumber(string areaCode, string phoneNumber)
        {
            if (areaCode == "0")
            {
                return areaCode + phoneNumber;
            }
            else
            {
                return "+" + areaCode + " " + phoneNumber;
            }
        }

        /// <summary>
        /// Format address to be displayed in the DataGridView
        /// </summary>
        /// <param name="addressLine1"></param>
        /// <param name="addressLine2"></param>
        /// <param name="addressLine3"></param>
        /// <returns></returns>
        protected static string FormatAddress(string addressLine1, string addressLine2, string addressLine3)
        {
            string address = addressLine1;

            if (addressLine2.Length > 0)
            {
                address += (Environment.NewLine + addressLine2);
            }

            if (addressLine3.Length > 0)
            {
                address += (Environment.NewLine + addressLine3);
            }

            return address;
        }

        /// <summary>
        /// Get value from the field, substitute an empty string for NULL values
        /// </summary>
        /// <param name="row"></param>
        /// <param name="index"></param>
        /// <returns></returns>
        protected static string GetNullableString(DataRow row, string index)
        {
            return row.IsNull(index) ? "" : row[index].ToString();
        }

        /// <summary>
        /// Format data retrieved from the database
        /// </summary>
        /// <param name="retrievedContacts"></param>
        /// <returns></returns>
        protected virtual DataTable FormatQueryResult(DataTable retrievedContacts)
        {
            return retrievedContacts;
        }

        /// <summary>
        /// Update data sorting configuration
        /// </summary>
        /// <param name="sortColumnIndex"></param>
        private void UpdateSortingInformation(int sortColumnIndex)
        {
            if (sortColumnIndex == _lastSortingColumnIndex)
            {
                _sortAscending = !_sortAscending;
            }
            else
            {
                ContactsListTable.Columns[_lastSortingColumnIndex].HeaderCell.SortGlyphDirection = SortOrder.None;
                _sortAscending = true;
                _lastSortingColumnIndex = sortColumnIndex;
                _lastSortingColumnName = ContactsListTable.Columns[_lastSortingColumnIndex].Name;
            }

            _updateSortingData = true;
        }

        /// <summary>
        /// Check if next page can be populated with data
        /// </summary>
        /// <param name="retrievedContacts"></param>
        private void UpdateNextPageStatus(DataTable retrievedContacts)
        {
            int retrievedRowsCount = retrievedContacts.Rows.Count;

            if (retrievedRowsCount > GetCurrentPerPageLimit())
            {
                retrievedContacts.Rows[retrievedRowsCount - 1].Delete();
                retrievedContacts.AcceptChanges();

                PaginateNextButton.Enabled = true;
            }
            else
            {
                PaginateNextButton.Enabled = false;
            }
        }

        /// <summary>
        /// Handle column header click
        /// </summary>
        /// <param name="sortColumnIndex"></param>
        protected void SortableColumnHeaderClicked(int sortColumnIndex)
        {
            UpdateSortingInformation(sortColumnIndex);

            UpdateContactsListData(true);
        }

        /// <summary>
        /// Handle change of the 
        /// </summary>
        protected void SearchChanged()
        {
            UpdateContactsListData(true);
        }

        /// <summary>
        /// Update DataGridView after number of contacts displayed per page has been changed
        /// </summary>
        protected void PerPageNumberChanged()
        {
            UpdateContactsListData(true);
        }

        /// <summary>
        /// Handle previous page button click
        /// </summary>
        protected void PrevButtonClicked()
        {
            int currentPage = GetCurrentPageNumber();

            PaginatePrevButton.Enabled = (--currentPage > 1);
            PaginateNextButton.Enabled = true;

            PaginatePageLabel.Text =  currentPage.ToString();

            UpdateContactsListData();
        }

        /// <summary>
        /// Handle next page button click
        /// </summary>
        protected void NextButtonClicked()
        {
            PaginatePageLabel.Text = (GetCurrentPageNumber() + 1).ToString();

            PaginatePrevButton.Enabled = true;

            UpdateContactsListData();
        }

        /// <summary>
        /// Handle edit button click
        /// </summary>
        protected void EditButtonClicked()
        {
            int selectedContact = GetSelectedRowId();

            if (selectedContact > 0)
            {
                if (ConfirmEdit() == DialogResult.Yes)
                {
                    EditContactControl.Open(selectedContact);
                }
            }
            else
            {
                ShowNoRowsSelectedWarning();
            }
        }

        /// <summary>
        /// Handle delete button click
        /// </summary>
        protected void DeleteButtonClicked()
        {
            int selectedContact = GetSelectedRowId();

            if (selectedContact > 0)
            {
                if (ConfirmDelete() == DialogResult.Yes)
                {
                    if (DeleteContact(selectedContact))
                    {
                        UpdateContactsListData();
                    }
                }
            }
            else
            {
                ShowNoRowsSelectedWarning();
            }
        }

        /// <summary>
        /// Show warning when the row isn't selected
        /// </summary>
        private static void ShowNoRowsSelectedWarning()
        {
            MessageBox.Show("Please select a row first!","Warning",  MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }

        /// <summary>
        /// Confirm edit operation
        /// </summary>
        /// <returns></returns>
        private static DialogResult ConfirmEdit()
        {
            return MessageBox.Show("Do you want to edit the contact?", "Edit", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
        }

        /// <summary>
        /// Confirm delete operation
        /// </summary>
        /// <returns></returns>
        private static DialogResult ConfirmDelete()
        {
            return MessageBox.Show("Do you want to delete the contact?", "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Stop);
        }

        /// <summary>
        /// Reset pagination controls
        /// </summary>
        private void ResetPagination()
        {
            PaginatePrevButton.Enabled = false;
            PaginateNextButton.Enabled = false;
            PaginatePageLabel.Text = "1";
        }

        /// <summary>
        /// Get number of currently displayed page (indexed from 1)
        /// </summary>
        /// <returns></returns>
        private int GetCurrentPageNumber()
        {
            return Int32.Parse(PaginatePageLabel.Text);
        }

        /// <summary>
        /// Get currently set limit of items displayed per page
        /// </summary>
        /// <returns></returns>
        private int GetCurrentPerPageLimit()
        {
            return (int) ContactsListPaginateSelect.SelectedValue;
        }

        /// <summary>
        /// Get ID of the selected row
        /// </summary>
        /// <returns></returns>
        private int GetSelectedRowId()
        {
            if (ContactsListTable.SelectedCells.Count > 0)
            {
                return Int32.Parse(
                    ContactsListTable.Rows[
                        ContactsListTable.SelectedCells[0].RowIndex
                    ].Cells[0].Value.ToString()
                );
            }
            else
            {
                return 0;
            }
        }

        /// <summary>
        /// Get list of contacts from the database
        /// </summary>
        /// <returns></returns>
        private DataTable GetContacts()
        {
            int limit = GetCurrentPerPageLimit();
            int offset = (GetCurrentPageNumber() - 1) * limit;

            return Contact.Get(
                ContactsListSearchBox.Text, 
                limit + 1, //return additional record to check if contents for next page are available
                offset, 
                _lastSortingColumnName, 
                _sortAscending
            );
        }

        /// <summary>
        /// Delete contact from the database
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        private bool DeleteContact(int id)
        {
            return Contact.Delete(id);
        }
    }
}
