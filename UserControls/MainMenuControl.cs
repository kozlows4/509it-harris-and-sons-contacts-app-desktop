﻿using System.Windows.Forms;
using _509it_harris_and_sons_contacts_app_desktop.Forms;

namespace _509it_harris_and_sons_contacts_app_desktop.UserControls
{
    public partial class MainMenuControl : UserControl
    {
        private readonly BaseForm _baseForm;

        public MainMenuControl(BaseForm form)
        {
            _baseForm = form;

            InitializeComponent();
        }

        public void Open()
        {
            _baseForm.TogglePanel(BaseForm.MainMenu);
            _baseForm.SetFormTitle("Harris & Sons Consulting LTD - Contact Management App");
        }
    }
}
