﻿
namespace _509it_harris_and_sons_contacts_app_desktop.UserControls
{
    partial class PersonalContactsListControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.contactsList = new System.Windows.Forms.DataGridView();
            this.id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.first_name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.last_name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.email = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.phone = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.address = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.postcode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.city = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.country = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.home_phone = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.contactsSearch = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.paginateSelect = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.editButton = new System.Windows.Forms.Button();
            this.deleteButton = new System.Windows.Forms.Button();
            this.paginateNext = new System.Windows.Forms.Button();
            this.paginatePageNumber = new System.Windows.Forms.Label();
            this.paginatePrev = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.contactsList)).BeginInit();
            this.SuspendLayout();
            // 
            // contactsList
            // 
            this.contactsList.AllowUserToAddRows = false;
            this.contactsList.AllowUserToDeleteRows = false;
            this.contactsList.AllowUserToResizeColumns = false;
            this.contactsList.AllowUserToResizeRows = false;
            this.contactsList.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.contactsList.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.contactsList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.contactsList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.id,
            this.first_name,
            this.last_name,
            this.email,
            this.phone,
            this.address,
            this.postcode,
            this.city,
            this.country,
            this.home_phone});
            this.contactsList.Location = new System.Drawing.Point(3, 137);
            this.contactsList.Name = "contactsList";
            this.contactsList.ReadOnly = true;
            this.contactsList.RowHeadersWidth = 51;
            this.contactsList.RowTemplate.Height = 24;
            this.contactsList.Size = new System.Drawing.Size(1335, 255);
            this.contactsList.TabIndex = 0;
            this.contactsList.ColumnHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.contactsList_ColumnHeaderMouseClick);
            // 
            // id
            // 
            this.id.DataPropertyName = "id";
            this.id.HeaderText = "Id";
            this.id.MinimumWidth = 6;
            this.id.Name = "id";
            this.id.ReadOnly = true;
            this.id.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            this.id.Visible = false;
            this.id.Width = 125;
            // 
            // first_name
            // 
            this.first_name.DataPropertyName = "first_name";
            this.first_name.HeaderText = "First Name";
            this.first_name.MinimumWidth = 6;
            this.first_name.Name = "first_name";
            this.first_name.ReadOnly = true;
            this.first_name.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            this.first_name.Width = 105;
            // 
            // last_name
            // 
            this.last_name.DataPropertyName = "last_name";
            this.last_name.HeaderText = "Last Name";
            this.last_name.MinimumWidth = 6;
            this.last_name.Name = "last_name";
            this.last_name.ReadOnly = true;
            this.last_name.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            this.last_name.Width = 105;
            // 
            // email
            // 
            this.email.DataPropertyName = "email";
            this.email.HeaderText = "Email";
            this.email.MinimumWidth = 6;
            this.email.Name = "email";
            this.email.ReadOnly = true;
            this.email.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            this.email.Width = 71;
            // 
            // phone
            // 
            this.phone.DataPropertyName = "phone";
            this.phone.HeaderText = "Phone";
            this.phone.MinimumWidth = 6;
            this.phone.Name = "phone";
            this.phone.ReadOnly = true;
            this.phone.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.phone.Width = 55;
            // 
            // address
            // 
            this.address.DataPropertyName = "address";
            this.address.HeaderText = "Address";
            this.address.MinimumWidth = 6;
            this.address.Name = "address";
            this.address.ReadOnly = true;
            this.address.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.address.Width = 66;
            // 
            // postcode
            // 
            this.postcode.DataPropertyName = "postcode";
            this.postcode.HeaderText = "Postcode";
            this.postcode.MinimumWidth = 6;
            this.postcode.Name = "postcode";
            this.postcode.ReadOnly = true;
            this.postcode.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.postcode.Width = 73;
            // 
            // city
            // 
            this.city.DataPropertyName = "city";
            this.city.HeaderText = "City";
            this.city.MinimumWidth = 6;
            this.city.Name = "city";
            this.city.ReadOnly = true;
            this.city.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.city.Width = 37;
            // 
            // country
            // 
            this.country.DataPropertyName = "country";
            this.country.HeaderText = "Country";
            this.country.MinimumWidth = 6;
            this.country.Name = "country";
            this.country.ReadOnly = true;
            this.country.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.country.Width = 63;
            // 
            // home_phone
            // 
            this.home_phone.DataPropertyName = "home_phone";
            this.home_phone.HeaderText = "Home Phone";
            this.home_phone.MinimumWidth = 6;
            this.home_phone.Name = "home_phone";
            this.home_phone.ReadOnly = true;
            this.home_phone.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.home_phone.Width = 96;
            // 
            // contactsSearch
            // 
            this.contactsSearch.Location = new System.Drawing.Point(1238, 109);
            this.contactsSearch.Name = "contactsSearch";
            this.contactsSearch.Size = new System.Drawing.Size(100, 22);
            this.contactsSearch.TabIndex = 1;
            this.contactsSearch.TextChanged += new System.EventHandler(this.contactsSearch_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(1175, 112);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(57, 17);
            this.label1.TabIndex = 2;
            this.label1.Text = "Search:";
            // 
            // paginateSelect
            // 
            this.paginateSelect.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.paginateSelect.FormattingEnabled = true;
            this.paginateSelect.Location = new System.Drawing.Point(73, 109);
            this.paginateSelect.Name = "paginateSelect";
            this.paginateSelect.Size = new System.Drawing.Size(121, 24);
            this.paginateSelect.TabIndex = 3;
            this.paginateSelect.SelectedIndexChanged += new System.EventHandler(this.paginateSelect_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 112);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(64, 17);
            this.label2.TabIndex = 4;
            this.label2.Text = "Paginate";
            // 
            // editButton
            // 
            this.editButton.Location = new System.Drawing.Point(6, 398);
            this.editButton.Name = "editButton";
            this.editButton.Size = new System.Drawing.Size(75, 23);
            this.editButton.TabIndex = 5;
            this.editButton.Text = "Edit";
            this.editButton.UseVisualStyleBackColor = true;
            this.editButton.Click += new System.EventHandler(this.editButton_Click);
            // 
            // deleteButton
            // 
            this.deleteButton.Location = new System.Drawing.Point(87, 398);
            this.deleteButton.Name = "deleteButton";
            this.deleteButton.Size = new System.Drawing.Size(75, 23);
            this.deleteButton.TabIndex = 6;
            this.deleteButton.Text = "Delete";
            this.deleteButton.UseVisualStyleBackColor = true;
            this.deleteButton.Click += new System.EventHandler(this.deleteButton_Click);
            // 
            // paginateNext
            // 
            this.paginateNext.Location = new System.Drawing.Point(1263, 398);
            this.paginateNext.Name = "paginateNext";
            this.paginateNext.Size = new System.Drawing.Size(75, 23);
            this.paginateNext.TabIndex = 7;
            this.paginateNext.Text = ">>";
            this.paginateNext.UseVisualStyleBackColor = true;
            this.paginateNext.Click += new System.EventHandler(this.paginateNext_Click);
            // 
            // paginatePageNumber
            // 
            this.paginatePageNumber.AutoSize = true;
            this.paginatePageNumber.Location = new System.Drawing.Point(1227, 401);
            this.paginatePageNumber.MinimumSize = new System.Drawing.Size(30, 0);
            this.paginatePageNumber.Name = "paginatePageNumber";
            this.paginatePageNumber.Size = new System.Drawing.Size(30, 17);
            this.paginatePageNumber.TabIndex = 8;
            this.paginatePageNumber.Text = "1";
            this.paginatePageNumber.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // paginatePrev
            // 
            this.paginatePrev.Location = new System.Drawing.Point(1146, 398);
            this.paginatePrev.Name = "paginatePrev";
            this.paginatePrev.Size = new System.Drawing.Size(75, 23);
            this.paginatePrev.TabIndex = 9;
            this.paginatePrev.Text = "<<";
            this.paginatePrev.UseVisualStyleBackColor = true;
            this.paginatePrev.Click += new System.EventHandler(this.paginatePrev_Click);
            // 
            // PersonalContactsListControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.paginatePrev);
            this.Controls.Add(this.paginatePageNumber);
            this.Controls.Add(this.paginateNext);
            this.Controls.Add(this.deleteButton);
            this.Controls.Add(this.editButton);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.paginateSelect);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.contactsSearch);
            this.Controls.Add(this.contactsList);
            this.Name = "PersonalContactsListControl";
            this.Size = new System.Drawing.Size(1341, 526);
            ((System.ComponentModel.ISupportInitialize)(this.contactsList)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView contactsList;
        private System.Windows.Forms.TextBox contactsSearch;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox paginateSelect;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button editButton;
        private System.Windows.Forms.Button deleteButton;
        private System.Windows.Forms.Button paginateNext;
        private System.Windows.Forms.Label paginatePageNumber;
        private System.Windows.Forms.Button paginatePrev;
        private System.Windows.Forms.DataGridViewTextBoxColumn id;
        private System.Windows.Forms.DataGridViewTextBoxColumn first_name;
        private System.Windows.Forms.DataGridViewTextBoxColumn last_name;
        private System.Windows.Forms.DataGridViewTextBoxColumn email;
        private System.Windows.Forms.DataGridViewTextBoxColumn phone;
        private System.Windows.Forms.DataGridViewTextBoxColumn address;
        private System.Windows.Forms.DataGridViewTextBoxColumn postcode;
        private System.Windows.Forms.DataGridViewTextBoxColumn city;
        private System.Windows.Forms.DataGridViewTextBoxColumn country;
        private System.Windows.Forms.DataGridViewTextBoxColumn home_phone;
    }
}
